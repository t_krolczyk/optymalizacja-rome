close all;
clearvars;
clc;

rome_begin ; % Rozpocz�cie pracy �rodowiska

h = rome_model ('zad GR2 '); % Utworzenie modelu


% Utworzenie zmiennych
newvar x integer 
newvar y integer;  

%funkcja celu
rome_maximize(17*x + 25*y);

%ograniczenia
rome_constraint(x + 6*y <= 60);
rome_constraint(2*x + 5*y <= 160);
rome_constraint(x >= 0);
rome_constraint(y >= 0);

% Rozwi�zywanie modelu i wyci�ganie rozwi�za�
h. solve ;
x_val = h. eval (x);
y_val = h. eval (y);


rome_end ; % Zamkni�cie �rodowiska ROM

%wypisanie wynik�w na konsol�
fprintf('\n\nZadanie 1\n');
fprintf('x_val r�wne jest %2.0f.\n', x_val);
fprintf('y_val r�wne jest %2.0f.\n', y_val);