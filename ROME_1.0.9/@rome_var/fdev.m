function val = fdev(obj)

% PROF_VAR\FDEV Returns the Forward Deviation of the uncertain variable
%
%
% Modified By:
% 1. Joel

val = obj.FDev;


% ROME: Copyright (C) 2009 by Joel Goh and Melvyn Sim
% See the file COPYING.txt for full copyright information.
