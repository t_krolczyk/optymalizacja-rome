function val = bdev(obj)

% PROF_VAR\BDEV Returns the Backward Deviation of the uncertain variable
%
%
% Modified By:
% 1. Joel

val = obj.BDev;


% ROME: Copyright (C) 2009 by Joel Goh and Melvyn Sim
% See the file COPYING.txt for full copyright information.
