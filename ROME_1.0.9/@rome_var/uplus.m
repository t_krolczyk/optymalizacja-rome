function A = uplus(A)

% ROME_VAR\UPLUS Implements unary plus in ROME
%
%   C = +A
%
%   A must be a rome_var object. Returns C, another rome_var object
%
% Modification History: 
% 1. Joel


% ROME: Copyright (C) 2009 by Joel Goh and Melvyn Sim
% See the file COPYING.txt for full copyright information.
