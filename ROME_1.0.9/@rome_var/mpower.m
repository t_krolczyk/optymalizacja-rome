function y = mpower(x, n)

% PROF_VAR\MPOWER Returns y such that x^n <= y
%
% Currently has the same functionality as power
%
% See also: power
% 
% Modified By:
% 1. Joel

y = power(x, n);



% ROME: Copyright (C) 2009 by Joel Goh and Melvyn Sim
% See the file COPYING.txt for full copyright information.
