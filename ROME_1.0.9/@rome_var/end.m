function end_length = end(A, k, n)

% ROME_VAR\END Overloads end keyword for indexing rome_var objects
%
%
% Modification History: 
% 1. Joel 

end_length = A.Size(k);

% ROME: Copyright (C) 2009 by Joel Goh and Melvyn Sim
% See the file COPYING.txt for full copyright information.
