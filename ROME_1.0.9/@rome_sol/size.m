function sz = size(sol_obj)
%
% ROME_SOL\SIZE Returns size of object
%
% Usage: 
% sz = size(x);
%
% History
% 1. Created by Joel 14 May 2009 
%

sz = sol_obj.Size;


% ROME: Copyright (C) 2009 by Joel Goh and Melvyn Sim
% See the file COPYING.txt for full copyright information.
