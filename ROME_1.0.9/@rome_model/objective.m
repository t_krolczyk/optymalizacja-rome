function obj_val = objective(model_obj)

% ROME_MODEL\OBJECTIVE Returns the objective of the model after solving
%
% Modified by:
% Joel

obj_val = model_obj.ObjVal;

