close all;
clearvars;
clc;

rome_begin ; % Rozpocz�cie pracy �rodowiska

h = rome_model ('zad 2LP '); % Utworzenie modelu

% Utworzenie zmiennych
U1 = newvar(1, 'integer');
U2 = newvar(1, 'integer');
U3 = newvar(1, 'integer');


% Funkcja celu
rome_maximize ( U1 + 3 * U2 + 2 * U3);

% Ograniczenia
rome_constraint (U1 + 2*U2 + U3 <= 5) ;
rome_constraint (U1 + U2 + U3 <= 4) ;
rome_constraint (U2 + 2*U3 <= 1) 
rome_constraint (U1 >= 0);
rome_constraint (U2 >= 0);
rome_constraint (U3 >= 0);
% Rozwi�zywanie modelu i wyci�ganie rozwi�za�
h. solve ;
U1_val = h. eval (U1);
U2_val = h. eval (U2);
U3_val = h. eval (U3);

rome_end ; % Zamkni�cie �rodowiska ROM

fprintf('\n\nZadanie 2\n');
fprintf('Urz�dzenia typu pierwszego nale�y wyprodukowa� sztuk %1.0f.\n', U1_val);
fprintf('Urz�dzenia typu drugiego nale�y wyprodukowa� sztuk %1.0f.\n', U2_val);
fprintf('Urz�dzenia typu trzeciego nale�y wyprodukowa� sztuk %1.0f.\n', U3_val);
