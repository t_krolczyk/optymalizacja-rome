close all;
clearvars;
clc;

%some comment
%suma poda�y towaru jest r�wna sumie popytu towaru, wi�c jest to
%zagadnienie transportowe zamkni�te (ZZT)


rome_begin ; % Rozpocz�cie pracy �rodowiska

h = rome_model ('zad 3TP '); % Utworzenie modelu

% Utworzenie zmiennych
xR = zeros(4,5);

x = newvar(4,5, 'integer'); 

%wsp�czynniki funkcji celu
tc = [5 3 1 2 2; 2 1 1 1 1; 1 1 2 5 2; 5 4 3 1 6];

% Funkcja celu
target = newvar(1, 'integer');
for i = 1 : 4
    for j = 1 : 5
        target = target + tc(i,j)*x(i,j);
    end
end
%minimalizujemy koszt transportu            
rome_minimize (target);
%ograniczenia na kolumny
iSum = [20 30 10 40];
for i = 1 : 4
    rome_constraint (x(i,1) + x(i,2) + x(i,3) + x(i,4) + x(i,5) == iSum(i));
end
%ograniczenia na wiersze
jSum = [10 15 30 10 35];
for j = 1 : 5
    rome_constraint (x(1,j) + x(2,j) + x(3,j) + x(4,j) == jSum(j));
end

%rozwi�zanie
h. solve ;
for i = 1 : 4
    for j = 1 : 5
        xR(i,j) = h. eval (x(i,j));
    end
end

rome_end ;
fprintf('\n\nZadanie 3\n');
fprintf('Warto�� dostaw poszczeg�lnych dostawc�w do odbiorc�w przedstawiona jest poni�ej \n \n');
fprintf('-- | O1 | O2 | O3 | O4 | O5\n');
for i = 1 : 4
    fprintf('D%1.0f | %2.0f | %2.0f | %2.0f | %2.0f | %2.0f\n', i, xR(i,1), xR(i,2), xR(i,3), xR(i, 4), xR(i,5));
end

