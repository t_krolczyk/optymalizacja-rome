close all;
clearvars;
clc;

rome_begin ; % Rozpocz�cie pracy �rodowiska

h = rome_model ('zad GR1 '); % Utworzenie modelu


% Utworzenie zmiennych
newvar x integer 
newvar y integer;  

%funkcja celu
rome_maximize(10*x + 5*y);

%ograniczenia
rome_constraint(4*x + 2*y <= 30);
rome_constraint(x + 4*y <= 90);
rome_constraint(x >= 0);
rome_constraint(y >= 0);

% Rozwi�zywanie modelu i wyci�ganie rozwi�za�
h. solve ;
x_val = h. eval (x);
y_val = h. eval (y);


rome_end ; % Zamkni�cie �rodowiska ROM

%wypisanie wynik�w na konsol�
fprintf('\n\nZadanie 1\n');
fprintf('x_val r�wne jest %2.0f.\n', x_val);
fprintf('y_val r�wne jest %2.0f.\n', y_val);