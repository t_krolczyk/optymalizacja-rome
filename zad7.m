close all;
clearvars;
clc;

rome_begin ; % Rozpocz�cie pracy �rodowiska

h = rome_model ('zad 7PL '); % Utworzenie modelu


% Utworzenie zmiennych
T32 = newvar(1, 'integer'); 
T36 = newvar(1, 'integer'); 
T42 = newvar(1, 'integer'); 

%funkcja celu
rome_maximize(250*T32 + 400*T36 + 500*T42);

%ograniczenia
rome_constraint(250*T32 + 400*T36 + 500*T42 >= 100000);
rome_constraint(0.5*T32 + T36 + 0.75*T42 <= 8000);
%Tel 32" ma mie� min 20% udzia��w rynku
rome_constraint(T32 >= 0.2 * (T32 + T36 + T42));
rome_constraint(T32 <= 1000);
rome_constraint(T36 >= 500);
rome_constraint(T32 >= 0);
rome_constraint(T36 >= 0);
rome_constraint(T42 >= 0);

h. solve ;
T32_val = h. eval (T32);
T36_val = h. eval (T36);
T42_val = h. eval (T42);
koszt = 250*T32_val + 400*T36_val + 500*T42_val;
czas = 0.5*T32_val + T36_val + 0.75*T42_val;

rome_end ;

fprintf('\n\nZadanie 7\n');
fprintf('Najbardziej optymalna ilo�� zakupu telewizor�w 32" to %2.0f.\n', T32_val);
fprintf('Najbardziej optymalna ilo�� zakupu telewizor�w 36" to %2.0f.\n', T36_val);
fprintf('Najbardziej optymalna ilo�� zakupu telewizor�w 42" to %2.0f.\n', T42_val);
fprintf('Maksymalny zysk ze sprzeda�y telewizor�w o okre�lonych wy�ej ograniczeniach to %2.0f z�otych.\n', koszt);
fprintf('Czas potrzebny na zrealizowanie tego planu przez lini� produkcyjn� wynosi %2.0f godzin.\n', czas);