close all;
clearvars;
clc;

rome_begin ; % Rozpocz�cie pracy �rodowiska

h = rome_model ('zad 5PL '); % Utworzenie modelu


% Utworzenie zmiennych
R1 = newvar(1, 'integer'); 
R2 = newvar(1, 'integer'); 

%funkcja celu
rome_minimize(7*R1 + 14*R2);

% Ograniczenia
rome_constraint(16*R1 + 48*R2 >= 48000);
rome_constraint(20*R1 + 10*R2 >= 20000);
%bo razem jest ich max 144 000HL, a musimy wyprodukowa� min 46k Pb95xD i
%20k ON wi�c 144k - 20k - 48k daje 76k
%no i musi by� mniej, bo im wi�cej benz albo ON tym mniej odpad�w
rome_constraint(24*R1 + 14*R2 <= 76000);
rome_constraint(R1 >= 0);
rome_constraint(R2 >= 0);

h. solve ;
r1_val = h. eval (R1);
r2_val = h. eval (R2);
koszt = 7*r1_val + 14*r2_val;

rome_end ;

fprintf('\n\nZadanie 5\n');
fprintf('Najbardziej optymalna ilo�� zakupu surowca R1 to %2.0f HL.\n', r1_val);
fprintf('Najbardziej optymalna ilo�� zakupu surowca R2 to %2.0f HL.\n', r2_val);
fprintf('Minimalny koszt zakupu surowca to %2.0f z�otych.\n', koszt);