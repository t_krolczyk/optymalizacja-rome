close all;
clearvars;
clc;

rome_begin ; % Rozpocz�cie pracy �rodowiska

h = rome_model ('zad 6PL '); % Utworzenie modelu


% Utworzenie zmiennych
C1 = newvar(1, 'integer'); 
C2 = newvar(1, 'integer'); 
C3 = newvar(1, 'integer');
C4 = newvar(1, 'integer'); 

%funkcja celu
rome_minimize(85*C1 + 70*C2 + 70*C3 + 80*C4);

%ograniczenia
rome_constraint(5*C1 + 2*C2 + 3*C3 + 2*C4 <= 1000);
rome_constraint(C1 + C3 >= 25);
rome_constraint(C2 + C4 >= 20);
%rome_constraint(0.7*C2 - 0.3*C1-0.3*C3-0.3*C4 <= 0);
rome_constraint(C2 <= 0.3 * (C1 + C2 + C3 + C4));
rome_constraint(C1 >= 0);
rome_constraint(C2 >= 0);
rome_constraint(C3 >= 0);
rome_constraint(C4 >= 0);

h. solve ;
C1_val = h. eval (C1);
C2_val = h. eval (C2);
C3_val = h. eval (C3);
C4_val = h. eval (C4);
koszt = 85*C1_val + 70*C2_val + 70*C3_val + 80*C4_val;
zuzycie = 5*C1_val + 2*C2_val + 3*C3_val + 2*C4_val;

rome_end ;

fprintf('\n\nZadanie 6\n');
fprintf('Liczba wyprodukowanych cz�ci numer 1 wynosi %2.0f.\n', C1_val);
fprintf('Liczba wyprodukowanych cz�ci numer 2 wynosi %2.0f.\n', C2_val);
fprintf('Liczba wyprodukowanych cz�ci numer 3 wynosi %2.0f.\n', C3_val);
fprintf('Liczba wyprodukowanych cz�ci numer 4 wynosi %2.0f.\n', C4_val);
fprintf('Minimalny koszt wyprodukowania tych cz�ci to %2.0f z�otych.\n', koszt);
fprintf('Do realizacji produkcji potrzebne b�dzie %2.0f sztuk p�fabrykatu.\n', zuzycie);