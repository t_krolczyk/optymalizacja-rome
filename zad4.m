close all;
clearvars;
clc;

rome_begin ; % Rozpoczęcie pracy środowiska

h = rome_model ('zad 4 '); % Utworzenie modelu


% Utworzenie zmiennych
xR = zeros(15,12);
x = newvar(15,12, 'binary'); 

O = [4 11 33 2 38 30 20 24 10 19 39 22 21 10 20];
Z = [5 107 110 96 12 32 41 82 17 87 13 79];
i = 1:15;
j = 1:12;



% Funkcja celu
rome_minimize (sumElements(x, O, Z));

%ograniczenia
for j = 1 : 12
    rome_constraint(x(1,j)+x(2,j)+x(3,j)+x(4,j)+x(5,j)+x(6,j)+x(7,j)+x(8,j)+x(9,j)+x(10,j)+x(11,j)+x(12,j)+x(13,j)+x(14,j)+x(15,j) == 1);
end
for i = 1 : 15
    rome_constraint(x(i,1)+x(i,2)+x(i,3)+x(i,4)+x(i,5)+x(i,6)+x(i,7)+x(i,8)+x(i,9)+x(i,10)+x(i,11)+x(i,12) <= 1);
end



h. solve ;
for i = 1 : 15
    for j = 1 : 12
        xR(i,j) = h. eval (x(i,j));
    end
end

rome_end ;

results = zeros(15,1);
for i = 1 : 15
    for j = 1 : 12
        if xR(i,j) == 1
            results(i) = j; 
        end
    end
end

fprintf('\n\nZadanie 4\n');
for i = 1 : 15
    fprintf('miejsce %2.0f jest zajmowanie przez artykuł numer %2.0f \n', i, results(i));
end

