rome_begin ; % Rozpocz�cie pracy �rodowiska
h = rome_model ('Simple LP '); % Utworzenie modelu

% Utworzenie zmiennych
newvar x y; 

% Funkcja celu
rome_maximize (30 * x + 40 * y);

% Ograniczenia
rome_constraint (16*x + 24*y <= 96000) ;
rome_constraint (16*x + 10*y <= 80000) ;
rome_constraint (x >= 0);
rome_constraint (y >= 0);
rome_constraint (x <= 3000);
rome_constraint (y <= 4000);
rome_constraint (y == (2/3) * x);
% Rozwi�zywanie modelu i wyci�ganie rozwi�za�
h. solve ;
x_val = h. eval (x);
y_val = h. eval (y);

rome_end ; % Zamkni�cie �rodowiska ROM