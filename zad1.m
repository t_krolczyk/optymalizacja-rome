close all;
clearvars;
clc;

rome_begin ; % Rozpocz�cie pracy �rodowiska
h = rome_model ('Simple LP '); % Utworzenie modelu

% tutaj tworzymy zmienne, mo�emy to zrobi� na 2 sposoby:
% np newvar x integer    
% lub (zapisy dzia�aj� tak samo)
% np x = newvar(1, 'integer');

newvar x integer 
newvar y integer; 

% funkcj� rome_maximize() nak�adamy funkcj� maksymalizacji celu
rome_maximize (12 * x + 15 * y);

% funkcj� rome_constraint() nak�adamy ograniczenia na model 
rome_constraint (x + 2*y <= 40) ;
rome_constraint (4*x + 3*y <= 120) ;
rome_constraint (x >= 0);
rome_constraint (y >= 0);

% Rozwi�zywanie modelu i wyci�ganie rozwi�za�
h. solve ;
x_val = h. eval (x);
y_val = h. eval (y);


rome_end ; % Zamkni�cie �rodowiska ROM

%wypisanie wynik�w na konsol�
fprintf('\n\nZadanie 1\n');
fprintf('x_val r�wne jest %2.0f.\n', x_val);
fprintf('y_val r�wne jest %2.0f.\n', y_val);